# Generated by Django 3.2 on 2021-05-03 03:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('empresa', '0005_alter_empresa_user_admin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empresa',
            name='user_admin',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='auth.user', verbose_name='Usuario Administrador'),
        ),
    ]
