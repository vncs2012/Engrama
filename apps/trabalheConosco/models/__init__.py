from .candidato import *
from .dadosEscolaridade import *
from .dadosPessoas import *
from .dadosProfissionais import *
from .questionarioPadrao import *
from .vagasCandidato import *