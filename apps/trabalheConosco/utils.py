SEXO = (
    (None, ''),
    ('M', 'Masculino'),
    ('F', 'Feminino'),
)

TP_ESCOLARIDADE = (
    (None, ''),
    (1, 'Fundamental - Incompleto'),
    (2, 'Fundamental - Completo'),
    (3, 'Médio - Incompleto'),
    (4, 'Médio - Completo'),
    (5, 'Superior - Incompleto'),
    (6, 'Superior - Completo'),
    (7, 'Pós-graduação (Lato senso) - Incompleto'),
    (8, 'Pós-graduação (Lato senso) - Completo'),
    (9, 'Pós-graduação (Stricto sensu, nível mestrado) - Incompleto'),
    (10, 'Pós-graduação (Stricto sensu, nível mestrado) - Completo'),
    (11, 'Pós-graduação (Stricto sensu, nível doutor) - Incompleto'),
)

STATUS_ESCOLARIDADE = (
    (None, ''),
    (1, 'Incompleto'),
    (2, 'Completo'),
)

TP_ESTADO_CIVIL = (
    (None, ''),
    (1, 'Solteiro'),
    (2, 'Casado'),
    (3, 'Separado'),
    (4, 'Divorciado'),
    (5, 'Viúvo'),
    (6, 'Amasiado'),
)

UF_ESTADOS = (
    ('AC', 'Acre'), 
    ('AL', 'Alagoas'),
    ('AP', 'Amapá'),
    ('BA', 'Bahia'),
    ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'),
    ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'),
    ('MA', 'Maranão'),
    ('MG', 'Minas Gerais'),
    ('MS', 'Mato Grosso do Sul'),
    ('MT', 'Mato Grosso'),
    ('PA', 'Pará'),
    ('PB', 'Paraíba'),
    ('PE', 'Pernanbuco'),
    ('PI', 'Piauí'),
    ('PR', 'Paraná'),
    ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'),
    ('RO', 'Rondônia'),
    ('RR', 'Roraima'),
    ('RS', 'Rio Grande do Sul'),
    ('SC', 'Santa Catarina'),
    ('SE', 'Sergipe'),
    ('SP', 'São Paulo'),
    ('TO', 'Tocantins')
)
