# Generated by Django 3.2.7 on 2021-10-03 20:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trabalheConosco', '0003_alter_candidato_cpf'),
    ]

    operations = [
        migrations.AddField(
            model_name='dadospessoas',
            name='data_nascimento',
            field=models.DateField(null=True, verbose_name='Data Nascimento'),
        ),
        migrations.AlterField(
            model_name='candidato',
            name='cpf',
            field=models.CharField(max_length=14, unique=True, verbose_name='CPF:'),
        ),
        migrations.AlterField(
            model_name='dadospessoas',
            name='sexo',
            field=models.CharField(choices=[(None, ''), ('M', 'Masculino'), ('F', 'Feminino')], max_length=1, verbose_name='Sexo:'),
        ),
    ]
