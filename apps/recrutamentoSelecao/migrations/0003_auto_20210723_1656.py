# Generated by Django 3.2 on 2021-07-23 19:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recrutamentoSelecao', '0002_auto_20210723_1655'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vagas',
            name='locacao',
        ),
        migrations.RemoveField(
            model_name='vagas',
            name='nome_vaga',
        ),
        migrations.AddField(
            model_name='vagas',
            name='cargo',
            field=models.OneToOneField(default='', on_delete=django.db.models.deletion.CASCADE, to='recrutamentoSelecao.cargos', verbose_name='Cargo'),
            preserve_default=False,
        ),
    ]
