from django.apps import AppConfig


class RecrutamentoselecaoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'recrutamentoSelecao'
    verbose_name = 'Recrutamento e Seleção'
