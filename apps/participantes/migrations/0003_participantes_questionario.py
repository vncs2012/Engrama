# Generated by Django 3.2 on 2021-05-03 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('participantes', '0002_alter_participantes_empresa'),
    ]

    operations = [
        migrations.AddField(
            model_name='participantes',
            name='questionario',
            field=models.BooleanField(blank=True, default=False, null=True, verbose_name='Respondeu?'),
        ),
    ]
