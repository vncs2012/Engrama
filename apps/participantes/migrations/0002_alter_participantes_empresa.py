# Generated by Django 3.2 on 2021-05-03 03:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('empresa', '0006_alter_empresa_user_admin'),
        ('participantes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participantes',
            name='empresa',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='empresa.empresa', verbose_name='Empresa Relacionada'),
        ),
    ]
